﻿    program $1dFDM

    implicit none

    interface
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Функция коэф. теплопроводности
    real function lambda(T)     
    REAL, INTENT(IN) :: T  
    end function lambda
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Функция удельной теплоемкости
    real function cFunction(T)    
    REAL, INTENT(IN) :: T
    end function cFunction
    
    end interface
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Variables
    integer :: i, j, N, s
    real :: ai, bi, ci, fi, max1, max2
    real :: ro, deltax, deltatau
    ! Граничные условия
    real :: Th, T0, Tc, L, t_end, time
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Создание массива температур
    ! real, dimension(N-2) :: T
    ! Тn - предыдущий временной слой
    ! Ts - предыдущая итерация
    real, dimension(500) :: T, Ts, Tn, alfa, beta

    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Ввод значений
    print *, 'Введите количество узлов, N'
    read *, N
    print *, 'Введите конечное значение времени, t_end'
    read *, t_end
    print *, 'Введите толщину слоя, L'
    read *, L
    print *, 'Введите плотность, ro'
    read *, ro
    print *, 'Введите начальную температуру в K, T0'
    read *, T0
    print *, 'Введите температуру в K  на границе x=0, Th'
    read *, Th
    print *, 'Введите температуру  в K  на границе x=L, Tc'
    read *, Tc

    
    ! Расчет шагов по пространству и времени
    deltax = L / (N-1)
    deltatau = t_end / 100.0

    ! Определяем поле температуры в начальный момент времени
    do I = 2, N - 1
        T(I)=T0
    end do

    ! Интегрирование нестрационарного уравнения теплопроводности
    time = 0
    do while(time < t_end)
        ! Увеличиваем время
        time = time + deltatau
        
        ! Запомнинаем поле температуры на предыдущем временном слое
        do I = 1, N
            Tn(I) = T(I)
        end do
        
        ! Число итераций
        s = 0
        
        !! Использовать until или while
        ! Цикл с постусловием для итерационного вычисления поля температур
        do while (max1/max2 >= 1E-5)
            ! Сначала расчет max и проверка для всей предыдущей итерации, потом переход к следующему слою
            ! if (max1/max2 <= 1E-5) exit 
            
            
            ! Определяем начальные прогоночные коэффициенты на основе левого граничного условия
            alfa(1)=0.0
            beta(1)=Th
        
            ! Прямая прогонка
            ! Цикл с параметром для определения прогоночных коэффициентов
            do I = 2, N-1
                ai = 0.5 * (lambda(T(I)) + lambda(T(I+1)))/(deltax * deltax)
                ci = 0.5 * (lambda(T(I-1)) + lambda(T(I)))/(deltax * deltax)
                ! Что с cFunction(T)?????????? только от I
                bi = ai + ci + ro * 0.5 * (cFunction(Tn(I)) + cFunction(Tn(I+1)))/deltatau
                fi = -ro * 0.5 *(cFunction(T(I)) + cFunction(T(I+1))) * Tn(I)/deltatau
            
                ! Прогоночные коэффициенты
                alfa(I) = ai/(bi-ci*alfa(I-1))
                beta(I) = (ci * beta(I-1) - fi)/(bi - ci * alfa(I-1))
            
            end do
            
            ! Температура на правой границе
            T(N) = Tc
            
            ! Обратная прогонка
            do I = N - 1, 1, -1
                T(I) = alfa(I) * T(I+1) + beta(I)
            end do
            
            ! Определяем максимальное по модулю значение разности температур
            max1 = abs(T(1) - Ts(1))
            do I = 2, N
                if (max1 < abs(T(I) - Ts(I))) max1 = abs(T(I) - Ts(I))
            end do
            
            ! Определяем максимальное по модулю значение температур
            max2 = abs(T(1))
            do I = 2, N
                if (max2 < abs(T(I))) max2 = abs(T(I))
            end do
            
            s = s + 1
            
            ! Запоминаем поле температуры на предыдущей итерации
            do I = 1, N
                Ts(I) = T(I)
            end do
                
           ! Запись количества циклов
            open(5, file = 'countIter.txt', status='replace')
            write(5, *) 'Количество итераций s =  ', s
            close(5) 
        end do ! Цикл с постусловием закончен
        
    end do ! Цикл с предусловием закончен
    
    open(1, file = 'startInfo.txt', status='replace')
    write(1, *) 'Толщина пластины L = ',L
    write(1, *) 'Число узлов по координате N = ',N
    write(1, *) 'Плотность материала пластины ro = ',ro
    write(1, *) 'Начальная температура T0 = ',T0
    write(1, *) 'Температура на границе x = 0, Th = ',Th
    write(1, *) 'Температура на границе x = L, Tc = ',Tc
    write(1, *) 'Результат получен с шагом по координате deltax = ',deltax
    write(1, *) 'Результат получен с шагом по времени deltatau = ',deltatau
    write(1, *) 'Температурное поле в момент времени t = ',t_end
    write(1, *) 'Количество итераций s = ', s
    close(1)
    
    open(2, file = 'temps.txt', status = 'replace')
    do I = 1, N
        write(2,*) T(I)-273,';'
    end do
    close(2)

    end program $1dFDM
    
    function lambda(T)
    implicit none
    real :: lambda
    real, intent(IN) :: T
    lambda = 100 * T
   ! lambda = 5500/(560+T)+0.942*(1E-10)*T*T*T
    end function lambda
    
    function cFunction(T)
    implicit none
    real :: cFunction
    real, intent(IN) :: T
    cFunction = 123 * T + 100
   ! cFunction = 5500/(560+T)+0.942*(1E-10)*T*T*T
    end function cFunction
    
    
