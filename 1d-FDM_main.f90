﻿    
    module charac
        use IVMRK_int
        include 'link_fnl_static.h'
        !!! Расчетный параметр
        ! Размер массивов для параметров материала
        integer, parameter :: paramArrSize = 100
    
        !!! Переменные свойств материала
        ! Переменные свойства материала зависящие от температуры
        real, dimension(paramArrSize) :: TM_arr, CM_arr, LamM_arr, roM_arr
        ! Количество строк с переменными свойствами материала
        integer :: NM
        external :: fcn
        ! Переменные свойства материала зависящие от температуры
        real, dimension(paramArrSize) :: TK_arr, CK_arr, LamK_arr, roK_arr
        ! Количество строк с переменными свойствами материала
        integer :: NK
        real :: lambdaOut, cOut, roOut
        
        !!! Параметры процесса
        ! Температуры начала и конца превращения
        real :: Ttransf, TendTransf, deltatau
        ! Параметры для расчета степени превращения
        real :: TinputFi, E, k, R
        
        ! Расчет энергии
        real :: Q0
        
    contains 
        
        ! Функция линейной интерполяции
        function linInter(Xinput, Xarr, Yarr, N_strings)
            implicit none
            real :: linInter
            real, intent(IN) :: Xinput
            real, dimension(N_strings), intent(in) :: Yarr, Xarr
            integer, intent(IN) :: N_strings
            real :: diffI, minDiff, secMinDiff, YI, YIPlus1, XI, XIPlus1, buff
            integer :: indexMinDiff, indexSecMinDiff, i
            
            if (Xinput <= Xarr(1) .OR. N_strings == 1) then
                linInter = Yarr(1)
                return
            end if
            
            if (Xinput >= Xarr(N_strings)) then
                linInter = Yarr(N_strings)
                return
            end if
            
            i = 1
            do while (Xinput >= Xarr(i))
                i = i + 1
                if (Xinput == Xarr(i)) then
                    linInter = Yarr(i)
                    return
                end if
            end do
            
            YI = Yarr(i-1)
            YIPlus1 = Yarr(i)
            XI = Xarr(i-1)
            XIPlus1 = Xarr(i)
             
            ! Формула линейной интерполяции
            linInter = YI + ((YIPlus1 - YI) / (XIPlus1 - XI)) * (Xinput - XI)
        end function linInter
        
        ! Расчет теплоты превращения
        function qv(fiCurr, Tinput)
            implicit none
            real, intent(IN) :: fiCurr, Tinput
            real :: qv
            
            qv = - Q0 * k * (1 - fiCurr) *  exp(- E / (R * Tinput))
        end function qv
        
        ! Определение характеристик материала по степени превращения и температуре
        subroutine characByTransf(T, fi, lambdaOut, cOut, roOut)
            real, intent(in) :: T, fi
            real, intent(out) :: lambdaOut, cOut, roOut
            
            real :: lambdaM, lambdaK, cM, cK, roM, roK
            
            lambdaM = linInter(T, TM_arr, lamM_arr, NM)
            cM = linInter(T, TM_arr, cM_arr, NM)
            roM = linInter(T, TM_arr, roM_arr, NM)
            lambdaK = linInter(T, TK_arr, lamK_arr, NK)
            cK = linInter(T, TK_arr, cK_arr, NK)
            roK = linInter(T, TK_arr, roK_arr, NK)
            
            if (fi <= 0.001) then
                lambdaOut = lambdaM
                cOut = cM
                roOut = roM
            else if (fi >= 0.999) then
                lambdaOut = lambdaK
                cOut = cK
                roOut = roK
            else 
                lambdaOut = fi * lambdaM + (1 - fi) * lambdaK
                cOut = fi * cM + (1 - fi) * cK
                roOut = fi * roM + (1 - fi) * roK
            end if

        end subroutine characByTransf
        
        ! Определение характеристик материала от температуры
        ! и предыдущего значения коэффициента превращения
        subroutine characByTemp(T, fiPrev, lambdaOut, cOut, roOut, fiCurr)
            real, intent(in) :: T
            real, intent(in) :: fiPrev
            real, intent(out) :: lambdaOut, cOut, roOut, fiCurr
            integer :: ido
            integer, parameter :: nDiff =1
            real :: tstart, tend, y(nDiff), yprime(nDiff)
            real :: buffer
            external fcn
            tstart = 0
            tend = deltatau
            y(1) = fiPrev
            ido = 1
            
            TinputFi = T
            
            ! Расчет коэффициента превращения от температуры
            if ( T <= Ttransf) then
                fiCurr = 0
            else if (T >= TendTransf) then
                fiCurr = 1
            else 
                !fiCurr = (T - Ttransf) * (1 / (TendTransf - Ttransf))
                call ivmrk(ido, fcn, tStart, tend, y, yprime)
                fiCurr = y(1)
                ido = 3
                call ivmrk(ido, fcn, tStart, tend, y, yprime)
            end if
                 
            if (fiCurr < fiPrev) then
                fiCurr = fiPrev
            end if
            
            call characByTransf(T, fiCurr, lambdaOut, cOut, roOut)
        end subroutine

    end module charac
    
    program $1dFDM
    use charac
    implicit none

    ! Переменные циклов
    integer :: i, j
    ! Количество узлов
    integer :: N
    ! Коэффиценты общего вида разностной схемы
    real :: ai, bi, ci, fi
    ! Переменные простой итерации
    real :: max_1, max_2
    real :: ro, sigma, deltax
    ! Граничные условия
    real :: Th, T0, Tc, L, t_end, time
    ! Итерационные переменные
    real :: lambdaTI, cT, lambdaTIPlus1, lambdaTIMinus1, lambda1, lambda2, cTn, cTs, cTi
    real :: alfaTs, betaTauS, gammaTs, deltaTs, Ts4
    ! Переменные для разбиения на множители уравнения теплопроводности
    real :: invMultip, coeffs
    
    !!! Граничные условия
    ! Переменные граничных условий
    integer :: k1
    integer :: kN
    ! nullable - вспомогательный нулевой массив
    ! BCtemp - массив температур для ГУ зависящих от T (должен совпадать для всех гу от Т)
    ! BCtau - массив времени для ГУ зависящих от времени (должен совпадать для всех гу от Tau)
    real, parameter :: tempBCarrSize = 100
    real, parameter :: tauBCarrSize = 1000
    real, dimension(tempBCarrSize) :: alfaT1, gammaT1, deltaT1, BCtemp, BCtau, nullable
    real, dimension(tauBCarrSize) :: betaTau1
    real, dimension(tempBCarrSize) :: alfaTN, gammaTN, deltaTN
    real, dimension(tauBCarrSize) :: betaTauN
    ! Количество строк для параметров от температуры
    integer :: NstrT
    ! Количество строк для параметров от времени
    integer :: NstrTau
    
    ! Счетчик количества итераций для: поля температур, температуры на левой границе, температуры на правой границе
    integer :: s, sT1, sTN
    ! Значения с предыдущей итерации для: поля температур, прогоночного коэффициента alfa(1), beta(1)
    ! левого граничного условия, температуры на правой границе
    real, dimension(100) :: Ts
    real :: T1s, TNs
    ! Точность цикла итерации
    real :: eps
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Создание массива температур
    ! T - рассчитываемый (нынешний) временной слой
    ! Тn - предыдущий временной слой
    ! alfa, beta - массивы прогоночных коэффициентов
    ! fi - коэффициент превращения
    real, parameter :: tempArrSize = 100
    real, dimension(tempArrSize) :: T, Tn, alfa, beta, fi_arr, fiN_arr, Qv_arr
    real :: fiT1s, fiTNs, fiT1, fiTN
    
    ! Буфферные переменные
    real :: buff_1, buffFi
    
    !!! Константы процесса
    ! Задание параметров для расчета степени превращения
    E = 13E+4
    k = 4.5E+7
    R = 8.341
    Q0 = 1.275E+5
    
    !!! Параметры расчета
    ! Задание параметров расчета
    N = 50
    t_end = 1000
    eps = 1E-3
    
    !!! Характеристики объекта
    ! Задание характеристик объекта
    L = 0.02
    sigma = 5.67 * 1E-8
    
    !!! Начальные условия
    ! Задание начальной температуры
    T0 = 300
    
    ! Заполнение массива свойств материала зависящих от температуры
    call Read3Column("data/inputData/bodyParams/roM.txt", TM_arr, roM_arr, nullable, NM)
    call Read3Column("data/inputData/bodyParams/roK.txt", TK_arr, roK_arr, nullable, NK)
    call Read3Column("data/inputData/bodyParams/materialParamM.txt", TM_arr, CM_arr, LamM_arr, NM)
    call Read3Column("data/inputData/bodyParams/materialParamK.txt", TK_arr, CK_arr, LamK_arr, NK)
    !!! Задать температуры превращения
    open(1, file = 'data/inputData/bodyParams/transformationTemp.txt',action='READ', status='OLD', form='FORMATTED')
    read(1, *)
    read(1, *) Ttransf
    read(1, *)
    read(1, *) TendTransf
    close(1)
    
    ! Задание граничных условий на левой границе
    open(1, file = 'data/inputData/initBoundCond/k1.txt',action='READ', status='OLD', form='FORMATTED')
    read(1, *)
    read(1, *) k1
    close(1)
    call Read3Column("data/inputData/initBoundCond/alfaT1.txt", BCtemp, alfaT1, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/deltaT1.txt", BCtemp, deltaT1, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/gammaT1.txt", BCtemp, gammaT1, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/betaTau1.txt", BCtau, betaTau1, nullable, NstrTau)
    ! Задание граничных условий на правой границе
    open(1, file = 'data/inputData/initBoundCond/kN.txt',action='READ', status='OLD', form='FORMATTED')
    read(1, *)
    read(1, *) kN
    close(1)
    call Read3Column("data/inputData/initBoundCond/alfaTN.txt", BCtemp, alfaTN, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/deltaTN.txt", BCtemp, deltaTN, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/gammaTN.txt", BCtemp, gammaTN, nullable, NstrT)
    call Read3Column("data/inputData/initBoundCond/betaTauN.txt", BCtau, betaTauN, nullable, NstrTau)
    
    ! Проверка корректности ГУ на правой границе
    if (kN == 0 .and. alfaTN(1) == 0) then
        print *, "Uncorrect BC"
        stop 1
        stop "Введите корректные ГУ - kN и alfaTN не могут быть одновременно равны 0"
    end if
    
    !!! Инициализация начальных значений
    ! Определяем поле температуры в начальный момент времени
    do I = 1, N 
        T(I)=T0
    end do
    
    ! Инициализируем поле температур для предыдущего временного слоя
    do I = 1, N
        Tn(I) = T0
    end do
    
    ! Инициализируем поле температур для предыдущей итерации
    do I = 1, N
        Ts(I) = T0
    end do
    
    do I = 1, N
        fi_arr(I) = 0
        fiN_arr(I) = 0
    end do
    
    fiT1s = 0
    fiTNs = 0
    fiT1 = 0
    fiTN = 0
    
    ! Инициализируем начальное значение времени
    time = 0
    
    ! Расчет шагов по пространству и времени
    deltax = L / (N-1)
    deltatau = 1
    !!! Конец блока инициализации

    ! Вывод в файл
    open(2, file = 'data/outputData/temps.txt', status = 'replace')
    write (2,*) 'time = ', time
    do I = 1, N, 5
        write(2,*) T(I), T(I+1), T(I+2), T(I+3), T(I+4)
    end do
    write (2,*) '---------------'
    
    ! Интегрирование нестрационарного уравнения теплопроводности
    do while(time < t_end)
        
        ! Вывод в файл
        write (2,*) 'time = ', time
        write (2,*) 'temps '
        do I = 1, N, 5
            write(2,*) T(I), T(I+1), T(I+2), T(I+3), T(I+4)
        end do
        write (2,*) 'fi '
        do I = 1, N, 5
            write(2,*) fi_arr(I), fi_arr(I+1), fi_arr(I+2), fi_arr(I+3), fi_arr(I+4)
        end do
        write (2,*) 'Qv '
        do I = 1, N, 5
            write(2,*) Qv_arr(I), Qv_arr(I+1), Qv_arr(I+2), Qv_arr(I+3), Qv_arr(I+4)
        end do
        write (2,*) '---------------'
        
        ! Увеличиваем время
        time = time + deltatau
        
        ! Запомнинаем поле температуры на предыдущем временном слое
        do I = 1, N
            Tn(I) = T(I)
        end do

        !!! Начальные значения для итераций по температуре
        ! Число итераций
        s = 0
        ! ???????
        max_1 = eps+1
        max_2 = 1
        
        ! Цикл с постусловием для итерационного вычисления поля температур
        do while (max_1/max_2 >= eps)
            
            sT1 = 0
            
            do while (.true.)
                sT1 = sT1 + 1
                T1s = T(1)

                ! ?? lamda(??), cT(??)
                if ((T(1) > 100000) .or. T(2) > 100000) then
                    open(2, file = 'data/outputData/error.txt', status = 'replace')
                    write (2,*) 'T(1) =', T(1)
                    write (2,*) 'T(2) =', T(2)
                    write (2, *) 'time =', time
                    write (2, *) 'sT1 = ', sT1
                    write (2, *) 'sTN = ', sTN
                    write (2, *) 's = ', s
                    close(2)
                    stop 100000
                end if
                
                !!!! Выделить ro
                call characByTemp(T(1), fiN_arr(1), lambda1, buff_1, ro, fi_arr(1))
                call characByTemp(T(2), fiN_arr(2), lambda2, buff_1, buff_1, fi_arr(2))
                !!!!!! fiPrev для Tn(I)
                buffFi = fiN_arr(I)
                call characByTemp(Tn(I), buffFi, buff_1, cTn, buff_1, fiN_arr(I))
                !!!!!! fiPrev и fiCurr для T1s
                buffFi = fiT1s
                call characByTemp(T1s, fiT1s, buff_1, cTs, buff_1, fiT1)
                
                lambdaTI = 0.5 * (lambda1 + lambda2)
                cT = 0.5 * (cTn + cTs)
                alfaTs = linInter(T1s, BCtemp, alfaT1, NstrT)
                gammaTs = linInter(T1s, BCtemp, gammaT1, NstrT)
                deltaTs = linInter(T1s, BCtemp, deltaT1, NstrT)
                betaTauS = linInter(time, BCtau, betaTau1, NstrTau)

                invMultip = (deltax * deltatau) / (k1 * ro * cT * (deltax ** 2) + 2 * deltatau * (k1 * lambdaTI + deltax * alfaTs + deltax * deltaTs * sigma * 4 * (Tn(1) ** 3))) 
                !!!! fiCurr для Tn(1)??
                coeffs = betaTauS + gammaTs + deltaTs * sigma * ((Tn(1) ** 4) - 4 * (Tn(1) ** 4))
                Qv_arr(1) =  qv(fiN_arr(1), Tn(1))
                
                if (k1 == 0 .and. alfaT1(1) == 0) then
                    alfa(1) = 1
                    beta(1) = 0
                else 
                    alfa(1) = 2 * k1 * lambdaTI * invMultip / deltax
                    beta(1) = invMultip * ((k1 * ro * cT * deltax * Tn(1) / deltatau) - (2 * coeffs) + (deltax * Qv_arr(1)))
                end if
                
                ! Прямая прогонка
                ! Цикл с параметром для определения прогоночных коэффициентов
                do I = 2, N-1
                    ! Запоминаем значения переменных, используемых > 1 раза для итерации
                    ! Запоминаем значение lambda(T(I))
                    call characByTemp(T(I), fiN_arr(I), lambdaTI, cTi, ro, fi_arr(I))
                    call characByTemp(T(I+1), fiN_arr(I+1), lambdaTIPlus1, buff_1, buff_1, fi_arr(I+1))
                    call characByTemp(T(I-1), fiN_arr(I-1), lambdaTIMinus1, buff_1, buff_1, fi_arr(I-1))
                    
                    !!!! fiPrev для Tn(I)?
                    buffFi = fiN_arr(I)
                    call characByTemp(Tn(I), buffFi, buff_1, cTn, buff_1, fiN_arr(I))
                    write(100,*)'fi(x) =', fi_arr(I)
                    
                    !lambdaTI = linInter(T(I), TM_arr, LamM_arr, NM)
                    !lambdaTIPlus1 = linInter(T(I+1), TM_arr, LamM_arr, NM)
                    !lambdaTIMinus1 = linInter(T(I-1), TM_arr, LamM_arr, NM)
                    ! Запоминаем значение cFunction(Tn(I)) + cFunction(T(I))
                    cT = 0.5 * (cTn + cTi)
                    
                    ai = 0.5 * (lambdaTI + lambdaTIPlus1)/(deltax * deltax)
                    ci = 0.5 * (lambdaTIMinus1 + lambdaTI)/(deltax * deltax)
                    bi = ai + ci + ro * cT/deltatau
                    !!! fi для Tn(I)
                    Qv_arr(I) =  qv(fiN_arr(I), Tn(I))
                    fi = (-ro * cT * Tn(I)/deltatau) - Qv_arr(I)
                    
                    ! Прогоночные коэффициенты
                    alfa(I) = ai/(bi-ci*alfa(I-1))
                    beta(I) = (ci * beta(I-1) - fi)/(bi - ci * alfa(I-1))
                    
                end do
                
                sTN = 0
                ! Итерации для нахождения температуры на правой границе
                do while (.true.) 
                    sTN = sTN + 1
                    TNs = T(N)
                    ! ?? lamda(??), cT(??)
                    call characByTemp(T(N), fiN_arr(N), lambda1, buff_1, ro, fi_arr(N))
                    call characByTemp(T(N-1), fiN_arr(N-1), lambda2, buff_1, buff_1, fi_arr(N-1))
                    !!!!! fiPrev для Tn??
                    buffFi = fiN_arr(N)
                    call characByTemp(Tn(N), buffFi, buff_1, cTn, buff_1, fiN_arr(N))
                    !!!! fiPrev для fiTN?
                    call characByTemp(TNs, fiTNs, buff_1, cTs, buff_1, fiTN)

                    lambdaTI = 0.5 * (lambda1 + lambda2)
                    cT = 0.5 * (cTn + cTs)
                    alfaTs = linInter(TNs, BCtemp, alfaTN, NstrT)
                    gammaTs = linInter(TNs, BCtemp, gammaTN, NstrT)
                    deltaTs = linInter(TNs, BCtemp, deltaTN, NstrT)
                    betaTauS = linInter(time, BCtau, betaTauN, NstrTau)
                    Qv_arr(N) =  qv(fiN_arr(N), Tn(N))
                    
                    invMultip = (2 * lambdaTI * deltatau) / ((kN * ro * cT * deltax * deltax) + 2 * deltatau * (kN * lambdaTI - kN * lambdaTI * alfa(N-1) + deltax * alfaTs + (deltax * deltaTs * sigma * 4 *(Tn(N) ** 3))))
                    
                    coeffs = betaTauS + gammaTs + (deltaTs * sigma * ((Tn(N) ** 4) - 4 * (Tn(N) ** 4)))
                    
                    T(N) = invMultip * (kN * beta(N-1) + ((kN * ro * cT * deltax * deltax)/(2 * lambdaTI * deltatau)) * Tn(N) - ((deltax / lambdaTI) * coeffs) + Qv_arr(N))

                    if (abs(TNs-T(N)) <= eps) then
                        exit
                    end if
                    if (sTN > 1000) then
                        stop N
                    end if
                end do
                
                ! Обратная прогонка
                do I = N - 1, 1, -1
                    T(I) = alfa(I) * T(I+1) + beta(I)
                end do
                
                ! Определяем максимальное по модулю значение разности температур
                max_1 = abs(T(1) - Ts(1))
                do I = 2, N
                    if (max_1 < abs(T(I) - Ts(I))) max_1 = abs(T(I) - Ts(I))
                end do
                
                ! Определяем максимальное по модулю значение температур
                max_2 = abs(T(1))
                do I = 2, N
                    if (max_2 < abs(T(I))) max_2 = abs(T(I))
                end do
                
                if (sT1 > 1000) then
                    open(2, file = 'data/outputData/error.txt', status = 'replace')
                    write (2,*) 'abs(T1s - T(1)) =', abs(T1s - T(1))
                    write (2, *) 'sT1 =', sT1
                    close(2)
                    stop 1
                end if
                
                ! Проверка условия итерации для alfa1, beta1
                if (abs(T1s - T(1)) <= eps) then
                    exit
                end if
                    
            end do
            
            s = s + 1
            if (s > 1000) then
                stop "Количество итераций по температурам больше 1000"
            end if
                
            ! Запоминаем поле температуры на предыдущей итерации
            do I = 1, N
                Ts(I) = T(I)
            end do
            
            do I = 1, N
                fiN_arr(I) = fi_arr(I)
            end do
                    
        end do ! Цикл с постусловием закончен
        
    end do ! Цикл с предусловием закончен
    
    ! Вывод параметров рассчета
    open(1, file = 'data/outputData/startInfo.txt', status='replace')
    write(1, *) 'Толщина пластины L = ',L
    write(1, *) 'Число узлов по координате N = ',N
    write(1, *) 'Плотность материала пластины ro = ',ro
    write(1, *) 'Начальная температура T0 = ',T0
    write(1, *) 'Температура на границе x = 0, Th = ',Th
    write(1, *) 'Температура на границе x = L, Tc = ',Tc
    write(1, *) 'Результат получен с шагом по координате deltax = ',deltax
    write(1, *) 'Результат получен с шагом по времени deltatau = ',deltatau
    write(1, *) 'Температурное поле в момент времени t = ',time
    close(1)
    
    ! Вывод в файл
    write (2,*) 'time = ', time
    write (2,*) 'temps '
    do I = 1, N, 5
        write(2,*) T(I), T(I+1), T(I+2), T(I+3), T(I+4)
    end do
    write (2,*) 'fi '
    do I = 1, N, 5
        write(2,*) fi_arr(I), fi_arr(I+1), fi_arr(I+2), fi_arr(I+3), fi_arr(I+4)
    end do
    write (2,*) 'Qv '
    do I = 1, N, 5
        write(2,*) Qv_arr(I), Qv_arr(I+1), Qv_arr(I+2), Qv_arr(I+3), Qv_arr(I+4)
    end do
    write (2,*) '---------------'
    close(2)
    
    contains
    
        ! Считывание трех колонок с файла
        subroutine Read3Column(fileName, firstCol, secCol, thirdCol, N)
            implicit none
            character(len = *), intent(in) :: fileName
            integer, intent(out) :: N
            real, intent(out), dimension(100) :: firstCol, secCol, thirdCol
            integer :: ios = 0, i
            logical :: fileIsExists
            
            open(1, file = fileName,action='READ', &
            status='OLD', form='FORMATTED', iostat=ios)
            
            N = 0
            ! считаем количество строк
            read(1, *) ! Пропускаем первую строчку
            do  
                read(1, *, iostat=ios)
                if (ios /= 0) exit
                N = N + 1
            end do
            
            rewind(1)

            read(1,*)
            do i = 1, N
                read(1,*) firstCol(i), secCol(i), thirdCol(i)
            end do
            
            close(1)
            
        end subroutine Read3Column
    
    end program $1dFDM